
# ARMA3 Clashpoint 2.0

[**Click here to download the latest .PBO(s)**](https://gitlab.com/dominicporteous/arma3-clashpoint2/-/jobs/artifacts/master/download?job=package)


---
### Welcome to Arma3 Clashpoint 2.0
Update to zBug's domination style game mode - now with a fundamental change to the way Clashpoint zones are generated and places, as well added support for AI squads, mid game air support, vanilla BIS arsenal.  

Features:  
- Solo, coop or PvP, the mission automatically adapts to the numbers of players on each side,  
- Dynamically generated firefights, unlimited situations,  
- 50 different locations on Altis, 300 different match setups,  
- Ground vehicles and air support,  
- Upgraded version of Farooq's revive script,  
- Allows the use of modded infantry gear (weapons, uniforms...),  
- Works in single player, on a local server and dedicated servers,  
  
Rules:  
- Each side starts the round with a 10 minute timer (configurable via Mission Parameters)  
- Holding the ClashPoint makes this timer tick down every second
- When it reaches zero, the side holding the flag wins. Which means it's not over until the clock rings!
- Unlimited respawns, spawn camping is made impossible by a spawn protection.
  
Required addons:  
- CBA_A3
  
[Click here to see the list of playable factions and the associated mods required.](#)