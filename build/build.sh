#!/bin/bash

set -e
mkdir -p "bin"

MISSION_NAME="Clashpoint"
MISSION_FOLDER_NAME="$MISSION_NAME"

for m in `cat ./build/maplist.txt`; do

  [[ -z "${m// }" ]] && continue;

  export MAP=$( echo "$m" | tr -cd 'A-Za-z0-9._-' )
  export MISSION_FOLDER="./build/$MISSION_FOLDER_NAME.$MAP"

  echo -e "Building $MAP..."

  mkdir -p "$MISSION_FOLDER" > /dev/null
  cp -r ./src/* "$MISSION_FOLDER" > /dev/null

  cat "./src/mission.sqm" | ./tools/bin/mo > "$MISSION_FOLDER/mission.sqm"
  cat "./src/description.ext" | ./tools/bin/mo > "$MISSION_FOLDER/description.ext"

  echo ""
  cat "$MISSION_FOLDER/description.ext"
  echo ""

  ./tools/bin/makepbo "$MISSION_FOLDER" "./bin/$MISSION_NAME.$MAP.pbo"

  echo -e "Building $MAP done."

done
